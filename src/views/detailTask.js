import React, { useState } from 'react';
import {
    SafeAreaView, StyleSheet, Image, TextInput,
    View, TouchableOpacity, Text, Modal,
} from 'react-native';
import Deadline from '../../components/Deadline';
import EditModal from '../../components/EditModal';

export default function App({ navigation, route }) {
    const [isModalVisible, setisModalVisible] = useState(false);
    const [tagData, settagData] = useState();
    const [todoData, settodoData] = useState();
    const [detailToDo, setdetailToDo] = useState({ id: '', toDo: '', tags: '' });
    const changeModalVisible = (bool) => {
        setisModalVisible(bool)
    }
    const setData = (data) => {
        settagData(data);
    }
    React.useEffect(() => {
        if (route.params?.toDo && route.params?.tags) {
            setdetailToDo({ id: route.params.id, toDo: route.params.toDo, tags: route.params.tags });
        }
    }, [route.params?.toDo, route.params?.tags]);

    return (
        <SafeAreaView style={styles.layout}>
            <View style={styles.controller}>

                {/* Go back button */}
                <TouchableOpacity onPress={() => {
                    navigation.goBack();
                }}>
                    <Image style={styles.backButton} source={require('../images/back-button.png')}></Image>
                </TouchableOpacity>

                {/* Input to do */}
                <View>
                    <Text style={styles.titleToDo}>To-do</Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Enter to do"
                        placeholderTextColor="white"
                        maxLength={25}
                        onChangeText={newText => settodoData(newText)}
                        defaultValue={detailToDo.toDo}
                    >
                    </TextInput>
                </View>

                {/* Input a tags */}
                <View>
                    <Text style={styles.title}>Tags</Text>
                    {
                        tagData ?
                            <Text style={styles.text}>{tagData}</Text>
                            :
                            <Text style={styles.text}>{detailToDo.tags}</Text>
                    }
                    <TouchableOpacity
                        style={styles.item}
                        onPress={() => changeModalVisible(true)}
                    >
                        <View style={styles.itemLeft}>
                            <Image source={require('../images/edit.png')}></Image>
                        </View>
                        <Text style={styles.text}>Edit tags</Text>

                    </TouchableOpacity>
                </View>

                {/* Modal */}
                <Modal
                    transparent={true}
                    animationType='fade'
                    visible={isModalVisible}
                    nRequestClose={() => changeModalVisible(false)}
                >
                    {
                        tagData ?
                            <EditModal
                                changeModalVisible={changeModalVisible}
                                setData={setData}
                                getData={tagData}
                            /> :
                            <EditModal
                                changeModalVisible={changeModalVisible}
                                setData={setData}
                                getData={detailToDo.tags}
                            />
                    }
                </Modal>

                {/* Deadline */}
                <Deadline />
            </View>

            {/* Button save */}

            <View style={styles.viewFooter}>
                <TouchableOpacity
                    style={styles.btnCancel}
                    onPress={() => navigation.navigate('Home', { idRemove: detailToDo.id, toDo: detailToDo.toDo, tags: detailToDo.tags })}
                >
                    <Image source={require('../images/cancel.png')}></Image>
                </TouchableOpacity>
                {
                    !todoData && !tagData ?
                    <TouchableOpacity
                            style={styles.btnDone}
                            onPress={() => navigation.navigate('Home', { idEdit: detailToDo.id, toDo: detailToDo.toDo, tags: detailToDo.tags })}
                        >
                            <Image source={require('../images/check.png')}></Image>
                        </TouchableOpacity> :
                    todoData && tagData ?
                        <TouchableOpacity
                            style={styles.btnDone}
                            onPress={() => navigation.navigate('Home', { idEdit: detailToDo.id, toDo: todoData, tags: tagData })}
                        >
                            <Image source={require('../images/check.png')}></Image>
                        </TouchableOpacity> :
                        todoData ?
                            <TouchableOpacity
                                style={styles.btnDone}
                                onPress={() => navigation.navigate('Home', { idEdit: detailToDo.id, toDo: todoData, tags: detailToDo.tags })}
                            >
                                <Image source={require('../images/check.png')}></Image>
                            </TouchableOpacity> :
                            <TouchableOpacity
                                style={styles.btnDone}
                                onPress={() => navigation.navigate('Home', { idEdit: detailToDo.id, toDo: detailToDo.toDo, tags: tagData })}
                            >
                                <Image source={require('../images/check.png')}></Image>
                            </TouchableOpacity>
                }

            </View>

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    layout: {
        flex: 1,
        backgroundColor: '#303030',
    },
    controller: {
        padding: 24,
    },
    backButton: {
        marginBottom: 50,
    },
    titleToDo: {
        color: '#FFF500',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
    },
    textInput: {
        color: '#FFF',
        fontWeight: '700',
        fontSize: 24,
        lineHeight: 29,
    },
    title: {
        color: '#FFF500',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
        marginTop: 31,
        marginBottom: 8,
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 31,
    },
    itemLeft: {
        width: 24,
        height: 24,
        backgroundColor: '#FFF',
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
    },
    text: {
        color: '#FFF',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
    },
    viewFooter: {
        flexDirection: 'row',
        height: 48,
        position: 'absolute',
        bottom: 32,
        left: 24,
        right: 24,
    },
    btnCancel: {
        width: 56,
        height: 56,
        backgroundColor: '#BF1616',
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnDone: {
        position: 'absolute',
        right: 0,
        width: 56,
        height: 56,
        backgroundColor: '#FFF',
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
});