import React, { useState } from 'react';
import {
  SafeAreaView, ScrollView, StyleSheet,
  Text, Image, TouchableOpacity, FlatList,
} from 'react-native';
import Task from '../../components/Task';
import Moment from 'moment';

let local_id = 5;
export default function App({ navigation, route }) {
  const getCurrentDate = Moment().format('DD MMMM YYYY');
  
  const [listToDo, setListToDo] = useState([
    { id: 1, toDo: 'Buy food !!!', tags: '#home' },
    { id: 2, toDo: 'Write Shopping list', tags: '#home' },
    { id: 3, toDo: 'Drive car to mechanic !!', tags: '#car' },
    { id: 4, toDo: 'Water flowers !', tags: '#home' },
    { id: 5, toDo: 'Read a book', tags: '#freetime' },
  ]);

  function deleteToDo(key) {
    const pos = listToDo.findIndex(item => item.id == key);
    listToDo.splice(pos, 1);

    setListToDo((preValue) => [...preValue]);
  }

  function editToDo(id, toDo, tags) {
    const newList = listToDo.map(item => {
      if (item.id == id) {
        item.toDo = toDo;
        item.tags = tags;
        return item;
      }
      return item;
    })

    setListToDo(newList);
  }

  React.useEffect(() => {
    if (route.params?.idRemove) {
      deleteToDo(route.params.idRemove);
    }
    if (route.params?.idEdit) {
      editToDo(route.params.idEdit, route.params.toDo, route.params.tags);
    }

    if (!route.params?.idRemove && !route.params?.idEdit && route.params?.toDo && route.params?.tags) {
      setListToDo((preValue) => [...preValue, { id: ++local_id, toDo: route.params.toDo, tags: route.params.tags }]);
    }
  }, [route.params?.toDo, route.params?.tags]);

  return (
    <SafeAreaView style={styles.layout}>
      {/* Current date */}
      <Text style={styles.titleDate}>{getCurrentDate}</Text>

      {/* Number of tasks */}
      <Text style={styles.titleTask}>{listToDo.length} Tasks</Text>

      {/* Whre tasks will go */}
      <FlatList
        data={listToDo}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => {
            navigation.navigate("DetailTask", { id: item.id, toDo: item.toDo, tags: item.tags });
          }}>
            <Task text={item.toDo} tag={item.tags} />
          </TouchableOpacity>
        )}
      />

      <TouchableOpacity style={styles.addTask}
        onPress={() => {
          navigation.navigate('AddTask');
        }}>
        <Image source={require('../images/add.png')}></Image>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    backgroundColor: '#FFF500',
    padding: 24,
  },
  titleDate: {
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 32,
    color: '#303030',
    marginBottom: 8,
    lineHeight: 38,
  },
  titleTask: {
    fontFamily: 'SF Pro',
    fontStyle: 'normal',
    fontWeight: '400',
    fontSize: 24,
    color: '#303030',
    marginBottom: 32,
    lineHeight: 29,
  },
  addTask: {
    position: 'absolute',
    bottom: 24,
    right: 24,
    width: 56,
    height: 56,
    backgroundColor: '#303030',
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
});