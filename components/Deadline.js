import React from "react";
import { View, Text, StyleSheet, Image} from 'react-native';

const Deadline = () => {
    
    return (
        <View>
            <Text style={styles.title}>Deadline</Text>
            <View style={styles.row}>
                <Image source={require('../src/images/calendar.png')}></Image>
                <Text style={styles.date}>Today</Text>
            </View>
        </View>
    ) 
};

const styles = StyleSheet.create({
    title: {
        color: '#FFF500',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
        marginBottom: 8,
    },
    row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignContent: 'center',
        alignItems: 'center',
    },
    date: {
        color: '#FFF',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 24,
        lineHeight: 29,
        paddingLeft: 8,
    }
});

export default Deadline;