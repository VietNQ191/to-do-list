import React from "react";
import { View, Text, StyleSheet} from 'react-native';

const Task = (props) => {
    
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}></View>
            <View>
                <Text style={styles.itemText}>{props.text}</Text>
                <Text style={styles.itemTag}>{props.tag}</Text>
            </View>
        </View>
    ) 
}

const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 24,
        alignItems: 'center',
    },
    itemLeft: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 32,
        height: 32,
        borderColor: 'rgba(0, 0, 0, 0.3)',
        borderWidth: 1,
        borderRadius: 60,
    },
    itemText: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 16,
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 24,
        lineHeight: 29,
        color: '#000',
    },
    itemTag: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 16,
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 16,
        lineHeight: 20,
        color: '#000',
    }
})

export default Task;