import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
} from 'react-native'
import Home from "./home";
import AddTask from "./addTask";
import DetailTask from "./detailTask";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default RootComponent = function () {
    return (
        <NavigationContainer>{}
        <Stack.Navigator initialRouteName='Home' screenOptions={{headerShown: false}}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="AddTask" component={AddTask} />
            <Stack.Screen name="DetailTask" component={DetailTask} />
        </Stack.Navigator>
      </NavigationContainer>
    );   
};