import React, { useState } from "react";
import {
    StyleSheet, Text, View, TextInput,
    TouchableOpacity,
} from 'react-native';

const EditModal = (props) => {
    const closeModal = (bool, data) => {
        props.changeModalVisible(bool);
        props.setData(data);
    }
    const [text, setText] = useState('');
   
    return (
        <TouchableOpacity
            disabled={true}
            style={styles.container}
        >
            <View style={styles.modal}>
                <View style={styles.textView}>

                    {/* Title */}
                    <Text style={styles.title}>Edit Tag</Text>
                    {/* Input add tags */}
                    <TextInput
                        style={styles.input}
                        placeholder="Add tags"
                        maxLength={25}
                        autoFocus={true}
                        onChangeText={newText => setText(newText)}
                        defaultValue={(props.getData).substring(1)}
                        color="#C5C5C5"
                    >
                    </TextInput>
                </View>

                <View style={styles.textView}>
                    {/* Edit button */}
                    <View style={styles.buttonsView}>
                        <TouchableOpacity
                            style={styles.btnRemove}
                            onPress={() => closeModal(false, " ")}
                            
                        >
                            <Text style={styles.txtRemove}>Remove</Text>
                        </TouchableOpacity>
                        {
                            text.length === 0 ?
                                <TouchableOpacity
                                    style={styles.btnSave}
                                    onPress={() => closeModal(false, (props.getData))}
                                >
                                    <Text style={styles.text}>Save</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity
                                    style={styles.btnSave}
                                    onPress={() => closeModal(false, "#" + text)}
                                >
                                    <Text style={styles.text}>Save</Text>
                                </TouchableOpacity>
                        }
                    </View>
                </View>

            </View>

        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modal: {
        height: 135,
        width: 335,
        paddingTop: 10,
        backgroundColor: '#FFF',
        borderRadius: 10,
    },
    title: {
        fontWeight: '700',
        fontSize: 20,
        lineHeight: 24,
        color: '#303030',
    },
    btnRemove: {
        flex: 1,
        paddingVertical: 20,
        position: 'absolute'
    },
    btnSave: {
        flex: 1,
        paddingVertical: 20,
        right: 0, 
        position: 'absolute',
    },
    textView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
    },
    buttonsView: {
        width: '100%',
        flexDirection: 'row',
    },
    text: {
        color: '#F6CF00',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
    },
    txtRemove: {
        color: '#BF1616',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 19,
    },
    input: {
        width: '100%',
        height: '100%',

    }
});

export default EditModal;