import React, { useState } from 'react';
import {
  SafeAreaView, StyleSheet, Image, TextInput,
  View, TouchableOpacity, Text, Modal,
} from 'react-native';
import Deadline from '../../components/Deadline';
import AddModal from '../../components/AddModal';

export default function App({ navigation }) {
  const [isModalVisible, setisModalVisible] = useState(false);
  const [tagData, settagData] = useState();
  const [todoData, settodoData] = useState();
  const changeModalVisible = (bool) => {
    setisModalVisible(bool)
  }
  const setData = (data) => {
    settagData(data);
  }

  return (
    <SafeAreaView style={styles.layout}>
      <View style={styles.controller}>

        {/* Go back button */}
        <TouchableOpacity onPress={() => {
          navigation.goBack();
        }}>
          <Image style={styles.backButton} source={require('../images/back-button.png')}></Image>
        </TouchableOpacity>

        {/* Input to do */}
        <View>
          <Text style={styles.titleToDo}>To-do</Text>
          <TextInput 
            style={styles.textInput} 
            placeholder="Enter to do" 
            placeholderTextColor="white" 
            maxLength={25}
            onChangeText={newText => settodoData(newText)}
            value={todoData}
          >
          </TextInput>
        </View>

        {/* Input a tags */}
        <View>
          <Text style={styles.title}>Tags</Text>
          <TouchableOpacity
            style={styles.item}
            onPress={() => changeModalVisible(true)}
          >
            <View style={styles.itemLeft}>
              <Image source={require('../images/add-dark.png')}></Image>
            </View>
            {
              tagData ?
                <Text style={styles.text}>{tagData}</Text>
                :
                <Text style={styles.text}>Add tags</Text>
            }

          </TouchableOpacity>
        </View>

        {/* Modal */}
        <Modal
          transparent={true}
          animationType='fade'
          visible={isModalVisible}
          nRequestClose={() => changeModalVisible(false)}
        >
          <AddModal
            changeModalVisible={changeModalVisible}
            setData={setData}
          />
        </Modal>

        {/* Deadline */}
        <Deadline />
      </View>

      {/* Button save */}
      {
        tagData == "Add tags"?
        settagData(null): null
      }
      <TouchableOpacity 
        style={styles.viewFooter}
        onPress={()=>navigation.navigate('Home', {toDo: todoData, tags: tagData})}
      >
        <Text style={styles.btnSave}>Save</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    backgroundColor: '#303030',
  },
  controller: {
    padding: 24,
  },
  backButton: {
    marginBottom: 50,
  },
  titleToDo: {
    color: '#FFF500',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
  },
  textInput: {
    color: '#FFF',
    fontWeight: '700',
    fontSize: 24,
    lineHeight: 29,
  },
  title: {
    color: '#FFF500',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    marginTop: 31,
    marginBottom: 8,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 31,
  },
  itemLeft: {
    width: 24,
    height: 24,
    backgroundColor: '#FFF',
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#FFF',
    fontStyle: 'normal',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19,
    marginLeft: 8,
  },
  viewFooter: {
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 32,
    left: 24,
    right: 24,
    borderRadius: 4,
    backgroundColor: '#F6CF00',
  },
  btnSave: {
    color: '#FFF',
    fontWeight: '700',
    fontSize: 18,
  }
});